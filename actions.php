<?php
    include('./db.php');

    $firstName = mysqli_real_escape_string($connection, $_POST["firstName"]);
    $lastName = mysqli_real_escape_string($connection, $_POST["lastName"]);
    $gender = mysqli_real_escape_string($connection, $_POST["gender"]);

    if(!isset($firstName) && empty($firstName)){
        echo "FirstName is required";
        exit();
    }
    if(!isset($lastName) && empty($lastName)){
        echo "LastName is required";
        exit();
    }
    if(!isset($gender) && empty($gender)){
        echo "Gender is required";
        exit();
    }
 
    $statement = "INSERT INTO `users` (firstname, lastname, gender) VALUES('$firstName', '$lastName', '$gender')";

    $query = mysqli_query($connection, $statement);

    if($query){
        echo json_encode('Submitted Successful');
    }else{
        echo json_encode('Submitted Failed');
    }

    mysqli_close($connection);

?>