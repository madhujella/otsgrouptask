
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <form class="form-horizontal" method="POST">
                        <div class="form-group">
                            <label for="firstName" class="col-sm-3 control-label">First Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="firstName" name="firstName" required  placeholder="First Name">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lastName" class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-9">

                                <input type="text" class="form-control" id="lastName" name="lastName" required  placeholder="Last Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Gender </label>
                            <div class="col-sm-9">
                                <label class="radio-inline">
                                    <input type="radio" id="male" name="gender" value="male" required >Male
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" id="female" name="gender" value="female" required >Female
                                </label>

                            </div>
                        </div>
                        <div class="col-sm-7 col-sm-offset-3">

                            <button type="submit" class="btn btn-default btn-success btn-block">Submit</button>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </section>

<div class="col-sm-4 col-sm-offset-4 text-center">
    <p class="alert alert-success" role="alert" id="alert"></p>
</div>    