$(document).ready(function(){

    $('form').submit(function(e){

        e.preventDefault();
        $('#alert').css('display', 'none');

        formData = {
            firstName: $('input[name=firstName]').val(),
            lastName: $('input[name=lastName]').val(),
            gender: $('input[name=gender]:checked').val()
        }

        console.log(formData);

        $.ajax({
            type: 'POST',
            url: 'actions.php',
            data: formData
        }).done(function(data){
            $('#alert').css("display", "block").html(data).delay(2000).fadeOut(500);
        }).fail(function(err){
            console.log(er);
        })
    })
})

